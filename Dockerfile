FROM debian:bookworm
LABEL maintainer="Patrick Rice"

ENV SAUCE_VERSION 4.8.2
ENV WORKING_DIR /usr/local/sauce-connect
ENV TUNNEL_NAME my-sauce-tunnel

WORKDIR ${WORKING_DIR}

RUN apt-get update && apt-get install -y wget

RUN wget https://saucelabs.com/downloads/sc-$SAUCE_VERSION-linux.tar.gz -O - | tar -xz

RUN mv sc-$SAUCE_VERSION-linux/* ./ && rm -rf sc-$SAUCE_VERSION-linux
RUN export SC=$WORKING_DIR

ENTRYPOINT ./bin/sc --tunnel-identifier "$TUNNEL_NAME"


